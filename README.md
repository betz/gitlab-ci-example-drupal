Examples

- Composer + npm + deploiement ssh server distant : https://docs.gitlab.com/ee/ci/examples/deployment/composer-npm-deploy.html

- Typo3 deployment example : https://github.com/smichaelsen/typo3-gitlab-ci/blob/master/src/.gitlab-ci.yml.dist

- Drupal phpcs example : http://cgit.drupalcode.org/dropfort_update/tree/.gitlab-ci.yml?id=170f01dbeeb60f4dc0768f20c84300adfc184b9f

- Drupal push to prod + dev : http://dev.studiopresent.com/blog/other/drupal-8-and-gitlab-continuous-integration-gitlab-ci

- Gitlab + D8 example : https://guillaumeduveau.com/fr/drupal/8/integration-continue-gitlab-ci-ansible


## Behat

- Behat + circle ci : https://github.com/leymannx/drupal-circleci-behat

- Behat specify tests in a sub directory : https://github.com/teamdeeson/d8-quickstart/blob/master/behat.yml


## Travis CI 

- Example drupal install : http://blog.freelygive.org.uk/2016/01/15/testing-with-travis-ci-on-github/