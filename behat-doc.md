Given /^(?:|I )am on "(?P<page>[^"]+)"$/
Given /^(?:|I )am on (?:|the )homepage$/
Given :type content:
Given :vocabulary terms:
Given I am an anonymous user
Given I am at :path
Given I am logged in as :name
Given I am logged in as a user with the :permissions permission(s)
Given I am logged in as a user with the :role role(s)
Given I am logged in as a user with the :role role(s) and I have the following fields:
Given I am not logged in
Given I am viewing a/an :type (content )with the title :title
Given I am viewing a/an :type( content):
Given I am viewing a/an :vocabulary term with the name :name
Given I am viewing my :type (content )with the title :title
Given I check the box :checkbox
Given I click :link in the :rowText row
Given I enter :value for :field
Given I fill in :field with :value in the :region( region)
Given I fill in :value for :field in the :region( region)
Given I press :button in the :region( region)
Given I press the :char key in the :field field
Given I run cron
Given I should not see the error message( containing) :message
Given I should not see the success message( containing) :message
Given I should not see the warning message( containing) :message
Given I uncheck the box :checkbox
Given I wait for AJAX to finish
Given a/an :type (content )with the title :title
Given a/an :vocabulary term with the name :name
Given for :field I enter :value
Given the cache has been cleared
Given users:

When /^(?:|I )additionally select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
When /^(?:|I )attach the file "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/
When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)"$/
When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
When /^(?:|I )fill in the following:$/
When /^(?:|I )follow "(?P<link>(?:[^"]|\\")*)"$/
When /^(?:|I )go to "(?P<page>[^"]+)"$/
When /^(?:|I )go to (?:|the )homepage$/
When /^(?:|I )move backward one page$/
When /^(?:|I )move forward one page$/
When /^(?:|I )press "(?P<button>(?:[^"]|\\")*)"$/
When /^(?:|I )reload the page$/
When /^(?:|I )select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)"$/
When I click :link
When I follow/click :link in the :region( region)
When I press the :button button
When I select the radio button :label
When I select the radio button :label with the id :id
When I visit :path

Then (I )break
Then /^(?:|I )should be on "(?P<page>[^"]+)"$/
Then /^(?:|I )should be on (?:|the )homepage$/
Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)"$/
Then /^(?:|I )should not see an? "(?P<element>[^"]*)" element$/
Then /^(?:|I )should not see text matching (?P<pattern>"(?:[^"]|\\")*")$/
Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
Then /^(?:|I )should see (?P<num>\d+) "(?P<element>[^"]*)" elements?$/
Then /^(?:|I )should see an? "(?P<element>[^"]*)" element$/
Then /^(?:|I )should see text matching (?P<pattern>"(?:[^"]|\\")*")$/
Then /^print current URL$/
Then /^print last response$/
Then /^show last response$/
Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should be checked$/
Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should not be checked$/
Then /^the "(?P<element>[^"]*)" element should contain "(?P<value>(?:[^"]|\\")*)"$/
Then /^the "(?P<element>[^"]*)" element should not contain "(?P<value>(?:[^"]|\\")*)"$/
Then /^the "(?P<field>(?:[^"]|\\")*)" field should contain "(?P<value>(?:[^"]|\\")*)"$/
Then /^the "(?P<field>(?:[^"]|\\")*)" field should not contain "(?P<value>(?:[^"]|\\")*)"$/
Then /^the (?i)url(?-i) should match (?P<pattern>"(?:[^"]|\\")*")$/
Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" (?:is|should be) checked$/
Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" is (?:unchecked|not checked)$/
Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" should (?:be unchecked|not be checked)$/
Then /^the response should contain "(?P<text>(?:[^"]|\\")*)"$/
Then /^the response should not contain "(?P<text>(?:[^"]|\\")*)"$/
Then /^the response status code should be (?P<code>\d+)$/
Then /^the response status code should not be (?P<code>\d+)$/
Then I (should ) see the :button button
Then I (should ) see the button :button
Then I (should )not see the heading :heading
Then I (should )see the :link in the :rowText row
Then I (should )see the heading :heading
Then I (should )see the text :text
Then I should be able to edit a/an :type( content)
Then I should get a :code HTTP response
Then I should not get a :code HTTP response
Then I should not see the following error messages:
Then I should not see the following success messages:
Then I should not see the following warning messages:
Then I should not see the link :link
Then I should not see the link :link in the :region( region)
Then I should not see the message( containing) :message
Then I should not see the text :text
Then I should not see( the text) :text in the :region( region)
Then I should not visibly see the link :link
Then I should see (the text ):text in the ":rowText" row
Then I should see the :heading heading in the :region( region)
Then I should see the error message( containing) :message
Then I should see the following error message(s):
Then I should see the following success messages:
Then I should see the following warning messages:
Then I should see the heading :heading in the :region( region)
Then I should see the link :link
Then I should see the link :link in the :region( region)
Then I should see the message( containing) :message
Then I should see the success message( containing) :message
Then I should see the warning message( containing) :message
Then I should see( the text) :text in the :region( region)
